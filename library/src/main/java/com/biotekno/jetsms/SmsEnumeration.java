package com.biotekno.jetsms;

public class SmsEnumeration {

    public enum MessageType {
        TEXT { public int getValue() { return 0; }  },
        BINARY { public int getValue() { return 1; } },
        WAPPUSH { public int getValue() { return 2; } },
        FLASH { public int getValue() { return 3; } },
        UNICODE { public int getValue() { return 4; } };

        public abstract int getValue();
    }

    public static MessageType ConvertMessageType(String g){
        MessageType messageType = SmsEnumeration.MessageType.TEXT;
        if(g.equals("0"))
            messageType = SmsEnumeration.MessageType.TEXT;
        else if (g.equals("1"))
            messageType = SmsEnumeration.MessageType.BINARY;
        else if (g.equals("2"))
            messageType = SmsEnumeration.MessageType.WAPPUSH;
        else if (g.equals("3"))
            messageType = SmsEnumeration.MessageType.FLASH;
        else if (g.equals("4"))
            messageType = SmsEnumeration.MessageType.UNICODE;
        return messageType;

    }

}
