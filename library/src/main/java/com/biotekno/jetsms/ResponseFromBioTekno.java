/*
 * Created on 15.May.2005
 */
package com.biotekno.jetsms;

/**
 * @author mustafa.bilge
 * @return The response of the posting request from the clients to Biotekno. 
 * It include the code and message of the response gotten from Biotekno.   
 */
public class ResponseFromBioTekno {
    
    private String code;
    private String message;
    
    public ResponseFromBioTekno(String code,String message){
        setCode(code);
        setMessage(message);
    }
    
    public ResponseFromBioTekno(){
        
    }
    
    public String toString(){
        return "Code:"+getCode()+"\n"+"Message:"+getMessage();
    }
        
    /**
     * @return Returns the code.
     */
    public String getCode() {
        return code;
    }
    /**
     * @param code The code to set.
     */
    public void setCode(String code) {
        this.code = code;
    }
    /**
     * @return Returns the message.
     */
    public String getMessage() {
        return message;
    }
    /**
     * @param message The message to set.
     */
    public void setMessage(String message) {
        this.message = message;
    }
}
