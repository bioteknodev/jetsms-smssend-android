/*
 * Created on 12.Eyl.2005
 */
package com.biotekno.jetsms;

public class MessageResponse {
    
    private String gsmNumber;
    private String status;
    private String sentDate;
    private String responseFromTelsimDate;
    

    public MessageResponse() {
        super();
    }
    
    public MessageResponse(String gsmNumber,String status) {
        setGsmNumber(gsmNumber);
        setStatus(status);
    }
    
    /**
     * @return Returns the gsmNumber.
     */
    public String getGsmNumber() {
        return gsmNumber;
        
    }

    /**
     * @param gsmNumber The gsmNumber to set.
     */
    void setGsmNumber(String gsmNumber) {
        this.gsmNumber = gsmNumber;
    }

    /**
     * @return Returns the status.
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status The status to set.
     */
    void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return Returns the getResponseFromTelsimDate.
     */
    public String getResponseFromTelsimDate() {
        return responseFromTelsimDate;
    }

    /**
     * @param getResponseFromTelsimDate The getResponseFromTelsimDate to set.
     */
    void setResponseFromTelsimDate(String getResponseFromTelsimDate) {
        this.responseFromTelsimDate = getResponseFromTelsimDate;
    }

    /**
     * @return Returns the sentDate.
     */
    public String getSentDate() {
        return sentDate;
    }

    /**
     * @param sentDate The sentDate to set.
     */
    void setSentDate(String sentDate) {
        this.sentDate = sentDate;
    }
    /**
     * @Override 
     */
    public String toString() {
        return getGsmNumber() + " " +  getStatus() + " " + getSentDate() + " " + getResponseFromTelsimDate(); 
    }
    
}
