/*
 * Created on 15.May.2005
 */
package com.biotekno.jetsms;


/**
 * @author mustafa.bilge
 * @return Use when sending an sms to a subscriber.
 */
public class SmsToOne extends SmsBase {
	private SmsMessage message = new SmsMessage();

	public String prepareMessage() throws SmsCreationException, SmsSendException {
		xmlToSend = "<?xml version=\"1.0\" encoding=\"iso-8859-9\" ?>" + "<message-context type=\"smmgsd\">" + super.createCommonXmlString();

		if (getMessage().getMessageType() != SmsEnumeration.MessageType.TEXT)
			xmlToSend += "<message-type>" + String.valueOf(getMessage().getMessageType().getValue()) + "</message-type>";

		if (getMessage().getMessageHeader() != null)
			xmlToSend += "<message-header>" + getMessage().getMessageHeader() + "</message-header>";

		xmlToSend += "<text>" + getMessage().getText() + "</text>" + "<message>" + "<gsmno>" + getMessage().getGsmNumber() + "</gsmno>" + "</message>" + "</message-context>";
		return xmlToSend;
	}

	public StringBuffer prepareMessageSB() throws SmsCreationException, SmsSendException {
		newMessageXML.append(this.prepareMessage());
		return newMessageXML;
	}

	public void sendMessage(JetSMSResult result) throws SmsCreationException, SmsSendException {
		newMessageXML = new StringBuffer();
		prepareMessageSB();
		postDataToBioTekno(result);
	}
	public void sendMessage(String baseUrl,JetSMSResult result) throws SmsCreationException, SmsSendException {
		newMessageXML = new StringBuffer();
		prepareMessageSB();
		postDataToBioTekno(baseUrl,result);
	}

	/**
	 * @return Returns the message.
	 */
	public SmsMessage getMessage() {
		return message;
	}

	/**
	 * @param message
	 *            The message to set.
	 */
	public void setMessage(SmsMessage message) {
		this.message = message;
	}

	public void setMessage(String gsmNumber, String text) {
		SmsMessage smsMessage = new SmsMessage(gsmNumber, text);
		this.message = smsMessage;
	}
}
