/*
 * Created on 15.May.2005
 */
package com.biotekno.jetsms;

/**
 * @author mustafa.bilge
 * @return The Exception gotten when creating Sms.
 */
public class SmsCreationException extends Exception {
    
    private static final long serialVersionUID = 20050515L;


    public SmsCreationException(String message){
        super(message);
    }
    
}
