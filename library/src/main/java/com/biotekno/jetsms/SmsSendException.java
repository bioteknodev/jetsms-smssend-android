/*
 * Created on 15.May.2005
 */
package com.biotekno.jetsms;

/**
 * @author mustafa.bilge
 */
public class SmsSendException extends Exception {

    /**
     * 
     */
    private static final long serialVersionUID = 20050515L;
    public SmsSendException(String message){
        super(message);
    }
}
