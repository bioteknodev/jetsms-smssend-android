package com.biotekno.jetsms;

public interface JetSMSResult {

    void OnSuccess(ResponseFromBioTekno resp);
    void OnFail(ResponseFromBioTekno resp);

}
