/*
 * Created on 15.May.2005
 */
package com.biotekno.jetsms;

import java.util.ArrayList;
/**
 * @author mustafa.bilge
 * @return Use when sending multi-sms i.e. send different message to different subscribers.
 */
public class SmsMultiSender extends SmsBase {

    private ArrayList<SmsMessage> smsMessages = new ArrayList<SmsMessage>();
    
    
    
    public String prepareMessage() throws SmsCreationException, SmsSendException {
        String messagesXml = "";
        
        for(int i=0;i<getSmsMessages().size();i++){
            SmsMessage smsMessage = getSmsMessages().get(i);
            
            messagesXml +=
                "<message>"+
                "<gsmno>"+smsMessage.getGsmNumber()+"</gsmno>";
            if(smsMessage.getMessageType() != SmsEnumeration.MessageType.TEXT) 
            	messagesXml += "<message-type>" + String.valueOf(smsMessage.getMessageType().getValue()) + "</message-type>";
                
            if(smsMessage.getMessageHeader()!=null)
            	messagesXml += "<message-header>"+ smsMessage.getMessageHeader()+"</message-header>";

                //"<message-type>"+String.valueOf(smsMessage.getMessageType())+"</message-type>"+
                //"<message-header>"+smsMessage.getMessageHeader()+"</message-header>"+
            messagesXml += "<text>"+smsMessage.getText()+"</text>"+
                "</message>";
            
        }
                
        xmlToSend = 
            "<?xml version=\"1.0\" encoding=\"iso-8859-9\" ?>" +
            "<message-context type=\"mmmgsd\">" +
            super.createCommonXmlString() +
            messagesXml +
            "</message-context>";
        return xmlToSend;
    }
    
    
    public StringBuffer prepareMessageSB() throws SmsCreationException, SmsSendException {
        newMessageXML.append("<?xml version=\"1.0\" encoding=\"iso-8859-9\" ?>");
        newMessageXML.append("<message-context type=\"mmmgsd\">");
        newMessageXML.append(super.createCommonXmlString());
        for(int i=0;i<getSmsMessages().size();i++){
            SmsMessage smsMessage = getSmsMessages().get(i);
            newMessageXML.append("<message>");
            newMessageXML.append("<gsmno>");
            newMessageXML.append(smsMessage.getGsmNumber());
            newMessageXML.append("</gsmno>");
            if(smsMessage.getMessageType() != SmsEnumeration.MessageType.TEXT) {
            	newMessageXML.append("<message-type>");
            		newMessageXML.append(String.valueOf(smsMessage.getMessageType().getValue()));
            	newMessageXML.append("</message-type>");
            }
                
            if(smsMessage.getMessageHeader()!=null){
            	newMessageXML.append("<message-header>");
            		newMessageXML.append(smsMessage.getMessageHeader());
            	newMessageXML.append("</message-header>");
            }
            newMessageXML.append("<text>");
            newMessageXML.append(smsMessage.getText());
            newMessageXML.append("</text>");
            newMessageXML.append("</message>");
            
        }
        newMessageXML.append("</message-context>");
        return newMessageXML;
    }

    public void sendMessage(JetSMSResult result)  throws SmsCreationException, SmsSendException {
        newMessageXML = new StringBuffer();
        prepareMessageSB();
        postDataToBioTekno(result);
    }

    public void sendMessage(String baseUrl,JetSMSResult result)  throws SmsCreationException, SmsSendException {
        newMessageXML = new StringBuffer();
        prepareMessageSB();
        postDataToBioTekno(baseUrl,result);
    }

    public void AddMessage(SmsMessage message){
       smsMessages.add(message); 
    }
    public void AddMessage(String gsmNumber, String text) {
        SmsMessage smsMessage = new SmsMessage(gsmNumber,text);
        AddMessage(smsMessage);
    }

    public void AddMessage(String gsmNumber,SmsEnumeration.MessageType messageType,String messageHeader,String text){
    	SmsMessage smsMessage = new SmsMessage(gsmNumber,messageType,messageHeader,text);
    	AddMessage(smsMessage);
    }
    /**
     * @return Returns the smsMessages.
     */
    public ArrayList<SmsMessage> getSmsMessages() {
        return smsMessages;
    }
    /**
     * @param smsMessages The smsMessages to set.
     */
    public void setSmsMessages(ArrayList<SmsMessage> smsMessages) {
        this.smsMessages = smsMessages;
    }
}
