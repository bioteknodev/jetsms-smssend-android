/*
 * Created on 15.May.2005
 */
package com.biotekno.jetsms;

import java.util.ArrayList;


/**
 * @author mustafa.bilge
 * @return Use when sending same Sms to different subscribers.
 */
public class SmsToMany extends SmsBase {

    private SmsEnumeration.MessageType messageType = SmsEnumeration.MessageType.TEXT;
    private String messageHeader = null;
    private String text;
    private ArrayList<String> gsmNumbers;

    public String prepareMessage() throws SmsCreationException, SmsSendException {
        String gsmNumbersXml = "";

        gsmNumbersXml = "<gsmnos>";
        for (int i = 0; i < getGsmNumbers().size(); i++) {
            gsmNumbersXml += getGsmNumbers().get(i) + ",";
        }
        gsmNumbersXml += "</gsmnos>";

        xmlToSend =
                "<?xml version=\"1.0\" encoding=\"iso-8859-9\" ?>" +
                        "<message-context type=\"smmgsd\">" +
                        super.createCommonXmlString();

        if (getMessageType() != SmsEnumeration.MessageType.TEXT)
            xmlToSend += "<message-type>" + String.valueOf(getMessageType().getValue()) + "</message-type>";

        if (getMessageHeader() != null)
            xmlToSend += "<message-header>" + getMessageHeader() + "</message-header>";

        xmlToSend += "<text>" + getText() + "</text>" +
                gsmNumbersXml +
                "</message-context>";

        return xmlToSend;
    }

    public StringBuffer prepareMessageSB() throws SmsCreationException, SmsSendException {
        StringBuffer gsmNumbersXml = new StringBuffer();

        gsmNumbersXml.append("<gsmnos>");
        for (int i = 0; i < getGsmNumbers().size(); i++) {
            gsmNumbersXml.append(getGsmNumbers().get(i) + ",");
        }
        gsmNumbersXml.append("</gsmnos>");

        newMessageXML.append("<?xml version=\"1.0\" encoding=\"iso-8859-9\" ?>");
        newMessageXML.append("<message-context type=\"smmgsd\">");
        newMessageXML.append(super.createCommonXmlString());

        if (getMessageType() != SmsEnumeration.MessageType.TEXT)
            newMessageXML.append("<message-type>" + String.valueOf(getMessageType().getValue()) + "</message-type>");

        if (getMessageHeader() != null)
            newMessageXML.append("<message-header>" + getMessageHeader() + "</message-header>");

        newMessageXML.append("<text>" + getText() + "</text>");
        newMessageXML.append(gsmNumbersXml);
        newMessageXML.append("</message-context>");
        return newMessageXML;
    }

    public void sendMessage(JetSMSResult result) throws SmsCreationException, SmsSendException {
        newMessageXML = new StringBuffer();
        prepareMessageSB();
        postDataToBioTekno(result);
    }

    public void sendMessage(String baseUrl,JetSMSResult result) throws SmsCreationException, SmsSendException {
        newMessageXML = new StringBuffer();
        prepareMessageSB();
        postDataToBioTekno(baseUrl,result);
    }

    /**
     * @return Returns the text.
     */
    public String getText() throws SmsCreationException {
        if (text == null)
            throw new SmsCreationException("Sms message text must be set in the message.");
        return text;
    }

    /**
     * @param text The text to set.
     */
    public void setText(String text) {
        this.text = text;
    }

    /**
     * @return Returns the gsmNumbers.
     */
    public ArrayList<String> getGsmNumbers() throws SmsCreationException {
        if (gsmNumbers == null || gsmNumbers.size() <= 0)
            throw new SmsCreationException("The GSM number(s) must be set in the message.");
        return gsmNumbers;
    }

    /**
     * @param gsmNumbers The gsmNumbers to set.
     */
    public void setGsmNumbers(ArrayList<String> gsmNumbers) {
        this.gsmNumbers = gsmNumbers;
    }

    public void setGsmNumbers(String[] gsmNumbers) {
        if (gsmNumbers == null)
            this.gsmNumbers = null;
        else {
            this.gsmNumbers = new ArrayList<String>();
            for (int i = 0; i < gsmNumbers.length; i++) {
                this.gsmNumbers.add(gsmNumbers[i]);
            }
        }
    }

    public String getMessageHeader() {
        return messageHeader;
    }

    public void setMessageHeader(String messageHeader) {
        this.messageHeader = messageHeader;
    }

    public SmsEnumeration.MessageType getMessageType() {
        return messageType;
    }

    public void setMessageType(SmsEnumeration.MessageType messageType) {
        this.messageType = messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = SmsEnumeration.ConvertMessageType(messageType);
    }

}
