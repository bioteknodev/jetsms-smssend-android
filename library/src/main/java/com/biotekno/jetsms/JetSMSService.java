package com.biotekno.jetsms;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface JetSMSService {

    @POST("SMS-Web/xmlsms")
    Call<ResponseBody> SendSMS(@Body RequestBody xmlPostData);
}
