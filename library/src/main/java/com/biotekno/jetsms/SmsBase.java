/*
 * Created on 15.May.2005
 */
package com.biotekno.jetsms;

import android.os.StrictMode;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import okio.BufferedSink;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

import java.io.IOException;
import java.util.Properties;

/**
 * @author mustafa.bilge
 * @return This class is not accessible from outside.
 */
abstract class SmsBase {
    protected String xmlToSend;
    private String username;
    private String password;
    private String outboxName;
    private String reference;
    private String processType = "API JAVA V1.0";
    private String startDate;
    private String expireDate;
    private String exclusionTimeStart;
    private String exclusionTimeStop;
    protected StringBuffer newMessageXML = new StringBuffer();

    public abstract void sendMessage(JetSMSResult result) throws SmsCreationException, SmsSendException;
    public abstract void sendMessage(String baseUrl,JetSMSResult result) throws SmsCreationException, SmsSendException;

    protected void postDataToBioTekno(JetSMSResult result) throws SmsSendException {
        sendToBiotekno(null, result);
    }

    protected void postDataToBioTekno(String baseUrl, JetSMSResult result) throws SmsSendException {
        sendToBiotekno(baseUrl, result);
    }

    private void sendToBiotekno(String url, final JetSMSResult result) throws SmsSendException {
        try {

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(url == null ? "http://service.jetsms.com.tr" : url)
                    .build();

            RequestBody body = RequestBody.create(MediaType.parse("text/xml"), newMessageXML.toString());

            JetSMSService jetSMSService = retrofit.create(JetSMSService.class);
            Call<ResponseBody> req = jetSMSService.SendSMS(body);

            req.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    ResponseFromBioTekno resp;
                    try {
                        String bodyStr = new String(response.body().bytes());
                        if (!bodyStr.equals("")) {
                            String code = bodyStr.substring(0, 2);
                            String message = bodyStr.substring(2).trim();
                            resp = new ResponseFromBioTekno(code, message);
                        } else {
                            resp = new ResponseFromBioTekno("99", "Could not connect to send servlet");
                        }
                    } catch (Exception ex) {
                        resp = new ResponseFromBioTekno("99", ex.getMessage());
                    }
                    if (resp.getCode().equals("00")) {
                        result.OnSuccess(resp);
                    } else {
                        result.OnFail(resp);
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    //Handle failure
                    result.OnFail(new ResponseFromBioTekno("99", t.getMessage()));
                }
            });
        } catch (Exception ex) {
            // throw new SmsSendException("Exception: " + ex.getMessage());
            result.OnFail(new ResponseFromBioTekno("99", ex.getMessage()));
        }
    }

    protected String createCommonXmlString() throws SmsCreationException {
        String optionalParams = "<username>" + getUsername() + "</username>" + "<password>" + getPassword() + "</password>" + "<outbox-name>" + getOutboxName() + "</outbox-name>";

        if (getReference() != null)
            optionalParams += "<reference>" + getReference() + "</reference>";
        if (getStartDate() != null)
            optionalParams += "<start-date>" + getStartDate() + "</start-date>";
        if (getExpireDate() != null)
            optionalParams += "<expire-date>" + getExpireDate() + "</expire-date>";
        if (getExclusionTimeStart() != null)
            optionalParams += "<exclusion-time-start>" + getExclusionTimeStart() + "</exclusion-time-start>";
        if (getExclusionTimeStop() != null)
            optionalParams += "<exclusion-time-stop>" + getExclusionTimeStop() + "</exclusion-time-stop>";
        if (getProcessType() != null) {
            optionalParams += "<process-type>" + getProcessType() + "</process-type>";
        }
        return optionalParams;
    }

    /**
     * @return Returns the expireDate.
     */
    public String getExpireDate() {
        return expireDate;
    }

    /**
     * @param expireDate The expireDate to set.
     */
    public void setExpireDate(String expireDate) {
        this.expireDate = expireDate;
    }

    /**
     * @return Returns the outboxName.
     */
    public String getOriginator() throws SmsCreationException {
        return getOutboxName();
    }

    /**
     * @return Returns the outboxName.
     */
    public String getOutboxName() throws SmsCreationException {
        if (outboxName == null)
            throw new SmsCreationException("OutboxName must be set in the message.");
        return outboxName;
    }

    /**
     * @param originator The originator which corresponds to outboxName to set.
     */
    public void setOriginator(String originator) {
        setOutboxName(originator);
    }

    /**
     * @param outboxName The outboxName to set.
     */
    public void setOutboxName(String outboxName) {
        this.outboxName = outboxName;
    }

    /**
     * @return Returns the password.
     */
    public String getPassword() throws SmsCreationException {
        if (password == null)
            throw new SmsCreationException("Password must be set in the message.");
        return password;
    }

    /**
     * @param password The password to set.
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return Returns the reference.
     */
    public String getReference() {
        return reference;
    }

    /**
     * @param reference The reference to set.
     */
    public void setReference(String reference) {
        this.reference = reference;
    }

    /**
     * @return Returns the startDate.
     */
    public String getStartDate() {
        return startDate;
    }

    /**
     * @param startDate The startDate to set.
     */
    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    /**
     * @return Returns the username.
     */
    public String getUsername() throws SmsCreationException {
        if (username == null)
            throw new SmsCreationException("Username must be set in the message.");
        return username;
    }

    /**
     * @param username The username to set.
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return Returns the xmlToSend.
     */
    public String getXmlToSend() {
        return xmlToSend;
    }

    public String getnewMessageXML() {
        return newMessageXML.toString();
    }

    /**
     * @return Returns the exclusionTimeStart.
     */
    public String getExclusionTimeStart() {
        return exclusionTimeStart;
    }

    /**
     * @param exclusionTimeStart The exclusionTimeStart to set.
     */
    public void setExclusionTimeStart(String exclusionTimeStart) {
        this.exclusionTimeStart = exclusionTimeStart;
    }

    /**
     * @return Returns the exclusionTimeStop.
     */
    public String getExclusionTimeStop() {
        return exclusionTimeStop;
    }

    /**
     * @param exclusionTimeStop The exclusionTimeStop to set.
     */
    public void setExclusionTimeStop(String exclusionTimeStop) {
        this.exclusionTimeStop = exclusionTimeStop;
    }

    private String getProcessType() {
        return processType;
    }

    private void setProcessType(String processType) {
        this.processType = processType;
    }

}
