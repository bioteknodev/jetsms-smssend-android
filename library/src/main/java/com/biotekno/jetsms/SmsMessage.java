/*
 * Created on 15.May.2005
 */
package com.biotekno.jetsms;

/**
 * @author mustafa.bilge
 * @return It simulates the mobile phone i.e.
 * the Gsm Number and the Sms message.
 */
public class SmsMessage {
    
    private String gsmNumber;
    private SmsEnumeration.MessageType messageType = SmsEnumeration.MessageType.TEXT;
    private String messageHeader = null;
    private String text;

    public SmsMessage(){
        
    }
    public SmsMessage(String gsmNumber, String text){
        setGsmNumber(gsmNumber);
        setText(text); 
    }
    public SmsMessage(String gsmNumber,SmsEnumeration.MessageType messageType,String messageHeader,String text){
        setGsmNumber(gsmNumber);
        setText(text);
        setMessageType(messageType);
        setMessageHeader(messageHeader);
    }
    
    /**
     * @return Returns the gsmNumber.
     * @throws SmsCreationException
     */
    public String getGsmNumber() throws SmsCreationException {
        if(gsmNumber==null)
            throw new SmsCreationException("Gsm Number must be set in the message.");

        return gsmNumber;
    }
    /**
     * @param gsmNumber The gsmNumber to set.
     */
    public void setGsmNumber(String gsmNumber) {
        this.gsmNumber = gsmNumber;
    }
    /**
     * @return Returns the text.
     * @throws SmsCreationException
     */
    public String getText() throws SmsCreationException {
        if(text==null)
            throw new SmsCreationException("Text must be set in the message.");
        return text;
    }
    /**
     * @param text The text to set.
     */
    public void setText(String text) {
        this.text = text;
    }
	public String getMessageHeader() {
		return messageHeader;
	}
	public void setMessageHeader(String messageHeader) {
		this.messageHeader = messageHeader;
	}
	public SmsEnumeration.MessageType getMessageType() {
		return messageType;
	}
	public void setMessageType(SmsEnumeration.MessageType messageType) {
		this.messageType = messageType;
	}
	public void setMessageType(String messageType) {
		this.messageType = SmsEnumeration.ConvertMessageType(messageType);
	}
	
}
